.PHONY: help

help:
	@echo 'coverage   : Run code coverage and get coverage result'
	@echo 'install    : Install requirements'
	@echo 'loaddata   : Load initial data'
	@echo 'migrations : Make django migrations'
	@echo 'run        : Run the project'
	@echo 'superuser  : Create a super user account on project'
	@echo 'test       : Run project tests'

coverage:
	@coverage run --source='.' manage.py test
	@coverage report -m

install:
	@pip install -r requirements.txt

loaddata:
	@python manage.py loaddata initial

migrations:
	@python manage.py makemigrations
	@python manage.py migrate

run:
	@python manage.py runserver 127.0.0.1:8000

superuser:
	@python manage.py createsuperuser

test:
	@python manage.py test
