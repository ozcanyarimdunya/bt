from datetime import timedelta

from .base import INSTALLED_APPS

DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': '123',
        'HOST': '127.0.0.1',
        'PORT': 5432
    }
}

INSTALLED_APPS += [
    'drf_yasg',
]

STATIC_URL = '/static/'

JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': timedelta(hours=5),
    'JWT_REFRESH_EXPIRATION_DELTA': timedelta(days=7),
}

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'EXCEPTION_HANDLER': 'api.contrib.handlers.api_exception_handler'
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

CACHE_QUERY_TIMEOUT = 86400

# We may have variables for base url and keys
# but no need to set extra variables
LOCATION_IQ_URL = 'https://eu1.locationiq.com/v1/search.php?key=a1779b7817b3b2&q={location}&format=json'
DARK_SKY_URL = 'https://api.darksky.net/forecast/f3146e0fc78b4930d41a60703c08e2ae/{lat},{lon}'

# Authorize with: JWT <token>
SWAGGER_SETTINGS = {
    'DEFAULT_INFO': 'bt.urls.info',
    'SECURITY_DEFINITIONS': {
        'JWT Authentication': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        }
    }
}
