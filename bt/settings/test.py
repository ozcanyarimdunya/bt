import os
from datetime import timedelta

DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('POSTGRES_NAME'),
        'USER': os.getenv('POSTGRES_USER'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD'),
        'HOST': os.getenv('POSTGRES_HOST'),
        'PORT': os.getenv('POSTGRES_PORT'),
    }
}

STATIC_URL = '/static/'

JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': timedelta(hours=5),
    'JWT_REFRESH_EXPIRATION_DELTA': timedelta(days=7),
}

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'EXCEPTION_HANDLER': 'api.contrib.handlers.api_exception_handler'
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": os.getenv('REDIS_LOCATION'),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

CACHE_QUERY_TIMEOUT = 86400

# We may have variables for base url and keys
# but no need to set extra variables
LOCATION_IQ_URL = 'https://eu1.locationiq.com/v1/search.php?key=a1779b7817b3b2&q={location}&format=json'
DARK_SKY_URL = 'https://api.darksky.net/forecast/f3146e0fc78b4930d41a60703c08e2ae/{lat},{lon}'
