import os

from .base import *

if os.getenv('MODE') == 'PRODUCTION':
    from .prod import *
elif os.getenv('MODE') == 'TEST':
    from .test import *
else:
    from .dev import *
