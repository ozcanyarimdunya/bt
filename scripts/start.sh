#!/bin/bash

echo "... Applying Django migrations"
python manage.py makemigrations
python manage.py migrate

echo "... Load initial data"
python manage.py loaddata initial

echo "... Starting Django"
exec gunicorn bt.wsgi -b 0.0.0.0:8888
