# Contributing

Follow the [PEP-8](https://www.python.org/dev/peps/pep-0008/) guide.

1. [settings](bt/settings) is the bootstrap folder you need to check, set up your configs.
2. Check and try running the tests
3. [views](api/views.py) is the endpoints holder file
4. Check the [models](api/models.py) and model manager
5. After that you will reach [contrib](api/contrib) modules
6. Try focusing on **FIXME** first :)
