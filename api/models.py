import logging

from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone

from api.contrib.handlers import CacheHandler
from api.contrib.requests import ApiRequest

logger = logging.getLogger()
User = get_user_model()


class WeatherManager(models.Manager):
    """
    Custom :Weather: model manager

    >>> Weather.objects.search(location='xx', user='<user-model>')
    """

    def search(self, location, user):
        """
        Search location

        > If cached get from cache
        > If exists in db today get from db
        > If none of above get from 3rd party api and save to both cache and db
        :param location: Any location name, city|place
        :param user: Authenticated user
        :return: Weather instance
        """
        from_cache = CacheHandler.get(location)
        if from_cache:
            CacheHandler.update(location)
            return from_cache

        today = timezone.localdate()
        from_db = self.get_queryset().filter(
            location=location,
            created_at__day=today.day,
            created_at__month=today.month,
            created_at__year=today.year
        ).first()
        if from_db:
            CacheHandler.set(location, from_db)
            return from_db

        data = ApiRequest(location=location).data()
        obj = self.create(**data, location=location, created_by=user)
        return obj


class Weather(models.Model):
    """
    Weather database model

    `location`      : location name info
    `temperature`   : Current temperature of location
    `today_lowest`  : Today's expected lowest temp, dark sky api mapping='currently.temperatureLow'
    `today_highest` : Today's expected highest temp, dark sky api mapping='currently.temperatureHigh'
    `weekly_lowest` : This week's expected lowest temp, dark sky api mapping='daily.data.temperatureLow'
    `weekly_highest`: This week's expected highest temp, dark sky api mapping='daily.data.temperatureHigh'
    `created_at`    : Created time, set automatically to time when model created
    `created_by`    : Create by, set to current authenticated user
    """

    location = models.CharField(
        max_length=150,
        verbose_name='Location name'
    )
    temperature = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        verbose_name='Current temperature'
    )
    today_lowest = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        verbose_name='Lowest temperature today',
        help_text='Expected lowest temperature for the remainder of the today',
    )
    today_highest = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        verbose_name='Highest temperature today',
        help_text='Expected highest temperature for the remainder of the today',
    )
    weekly_lowest = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        verbose_name='Lowest temperature this week',
        help_text='Expected lowest temperature in this week',
    )
    weekly_highest = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        verbose_name='Highest temperature this week',
        help_text='Expected highest temperature in this week',
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Created time',
    )
    created_by = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='+',
        verbose_name='Created by'
    )

    objects = WeatherManager()

    class Meta:
        ordering = ['-created_at']
        indexes = [
            models.Index(fields=['location'], name='weather_location_idx')
        ]

    def __str__(self):
        return self.location
