from rest_framework import status
from rest_framework.exceptions import APIException


class EmptyQueryException(APIException):
    """Empty query exception"""
    status_code = status.HTTP_417_EXPECTATION_FAILED
    default_detail = 'Location query cannot be empty'
    default_code = 'empty_query'
