from rest_framework import serializers

from api.models import Weather


class WeatherSerializers(serializers.ModelSerializer):
    """
    WeatherSerializers
    ~~~~~~~~~~~~~~~~~~

    Response data holder for end users
    """

    class Meta:
        model = Weather
        fields = (
            'temperature',
            'today_lowest',
            'today_highest',
            'weekly_lowest',
            'weekly_highest',
        )
