from django.contrib import admin
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import TokenProxy

from api.models import Weather

admin.site.site_header = 'BtWeather Administration'

admin.site.unregister(Group)
admin.site.unregister(TokenProxy)


@admin.register(Weather)
class WeatherAdmin(admin.ModelAdmin):
    list_display = (
        'location',
        'temperature',
        'today_lowest',
        'today_highest',
        'weekly_lowest',
        'weekly_highest',
        'created_by',
        'created_at',
    )
    search_fields = (
        'location',
        'created_by__username'
    )
    list_filter = (
        'created_by',
    )
