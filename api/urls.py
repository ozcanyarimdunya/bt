from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token as login_view

from api.views import SearchAPIView

app_name = 'api'
urlpatterns = [
    path('login/', login_view, name='login'),
    path('search/', SearchAPIView.as_view(), name='search'),
]
