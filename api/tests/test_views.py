import os
import time

from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.test import override_settings
from django.urls import reverse_lazy
from django.utils import timezone
from rest_framework.test import APITestCase

from api.models import Weather

User = get_user_model()


class TestLogin(APITestCase):

    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(username='test1', email='test1@test1.com')
        user.set_password('test1')
        user.save()

    def test_login(self):
        data = {
            'username': 'test1',
            'password': 'test1'
        }
        response = self.client.post(reverse_lazy('api:login'), data=data)
        self.assertIsNotNone(response.data.get('token'))


@override_settings(CACHES={
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": os.getenv("REDIS_LOCATION", "redis://127.0.0.1:6379/test"),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
})
class TestSearch(APITestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(username='test', email='test@test.com')
        user.set_password('test')
        user.save()

    @staticmethod
    def clear_cache():
        for i in cache.keys('*'):
            cache.delete(i)

    def setUp(self) -> None:
        self.clear_cache()
        data = {
            'username': 'test',
            'password': 'test'
        }
        response = self.client.post(reverse_lazy('api:login'), data=data)
        token = response.data['token']
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

    def tearDown(self) -> None:
        self.clear_cache()

    def test_search(self):
        url = "{}?location=Istanbul".format(reverse_lazy('api:search'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_search_empty_query(self):
        url = "{}?location=".format(reverse_lazy('api:search'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 417)

    def test_search_fail(self):
        url = "{}?location=---".format(reverse_lazy('api:search'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 500)

    def test_search_unauthorized(self):
        self.client.logout()
        url = "{}?location=Test".format(reverse_lazy('api:search'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)

    def test_search_from_cache(self):
        self.assertTrue(cache.keys('*') == list())
        self.assertEqual(Weather.objects.filter(location='Ankara').count(), 0)
        self.client.get("{}?location=Ankara".format(reverse_lazy('api:search')))
        self.assertEqual(Weather.objects.filter(location='Ankara').count(), 1)
        self.client.get("{}?location=Ankara".format(reverse_lazy('api:search')))
        self.assertEqual(Weather.objects.filter(location='Ankara').count(), 1)
        self.assertListEqual(cache.keys('*'), ['Ankara'])

    def test_search_from_db(self):
        with self.settings(CACHE_QUERY_TIMEOUT=1):
            self.client.get("{}?location=Bursa".format(reverse_lazy('api:search')))
            time.sleep(1.2)
            self.assertTrue(cache.keys('*') == list())
            self.client.get("{}?location=Bursa".format(reverse_lazy('api:search')))
            self.assertEqual(Weather.objects.filter(location='Bursa').count(), 1)

    def test_search_from_api(self):
        with self.settings(CACHE_QUERY_TIMEOUT=1):
            self.assertEqual(Weather.objects.filter(location='Adana').count(), 0)
            self.client.get("{}?location=Adana".format(reverse_lazy('api:search')))
            Weather.objects.get(location='Adana').delete()
            self.assertEqual(Weather.objects.filter(location='Adana').count(), 0)
            time.sleep(1.2)
            self.client.get("{}?location=Adana".format(reverse_lazy('api:search')))
            self.assertEqual(Weather.objects.filter(location='Adana').count(), 1)

    def test_search_within_today(self):
        obj = Weather.objects.create(
            location='Batman',
            temperature=89.00,
            today_lowest=88.00,
            today_highest=100.00,
            weekly_lowest=50.00,
            weekly_highest=150.00,
        )
        obj.created_at = timezone.now() - timezone.timedelta(days=1)
        obj.save()
        # note: signal is adding to cache, so delete cache or
        #  override settings for CACHE_QUERY_TIMEOUT and sleep.
        self.clear_cache()
        self.assertTrue(cache.keys('*') == list())
        self.client.get("{}?location=Batman".format(reverse_lazy('api:search')))
        self.assertEqual(Weather.objects.filter(location='Batman').count(), 2)

    def test_model_signal(self):
        for i in range(5):
            Weather.objects.create(
                location='test-{}'.format(i),
                temperature=89.00,
                today_lowest=88.00,
                today_highest=100.00,
                weekly_lowest=50.00,
                weekly_highest=150.00,
            )
        self.assertEqual(len(cache.keys('*')), 5)

    def test_cache_count_max_50(self):
        for i in range(55):
            Weather.objects.create(
                location='test-{}'.format(i),
                temperature=89.00,
                today_lowest=88.00,
                today_highest=100.00,
                weekly_lowest=50.00,
                weekly_highest=150.00,
            )
        self.assertEqual(len(cache.keys('*')), 50)
