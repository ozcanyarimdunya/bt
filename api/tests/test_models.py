from django.test import TestCase

from api.models import Weather


class TestWeather(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.obj = Weather.objects.create(
            location="Test",
            temperature=12.00,
            today_lowest=11.00,
            today_highest=13.00,
            weekly_lowest=9.00,
            weekly_highest=15.00,
        )

    def test_str(self):
        self.assertEqual(str(self.obj), 'Test')
