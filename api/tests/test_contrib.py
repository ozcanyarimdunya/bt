from urllib.error import HTTPError

from django.test import TestCase

from api.contrib.requests import ApiRequest
from api.contrib.utils import make_request


class TestUtils(TestCase):
    def test_make_request(self):
        url = 'https://jsonplaceholder.typicode.com/todos/1'
        response: dict = make_request(url)
        self.assertListEqual(
            sorted(response.keys()),
            sorted(['completed', 'id', 'title', 'userId'])
        )

        url = 'https://github.com/'
        response: str = make_request(url, serialize=False)
        self.assertIsInstance(response, str)


class TestRequests(TestCase):
    def test_api_request_success(self):
        data: dict = ApiRequest(location='Izmir').data()
        self.assertListEqual(
            sorted(data.keys()),
            sorted(['temperature', 'today_lowest', 'today_highest', 'weekly_lowest', 'weekly_highest'])
        )

    def test_api_request_fail(self):
        with self.assertRaises(HTTPError):
            ApiRequest(location='----').data()
