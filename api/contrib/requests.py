from django.conf import settings

from api.contrib.utils import make_request


class ApiRequest:
    """
    ApiRequest
    ~~~~~~~~~~
    3rd party api request handler

    >>> req = ApiRequest(location='xxx')
    >>> req.data()
    """

    def __init__(self, location):
        self._location = location

    def get_forecast(self) -> dict:
        """
        Forecast api handler
        1. Get lat, lon from location iq api
        2. By using given lat,lon get weather data from dark sky api
        :return: response dictionary
        """
        # Make request to location iq api
        url = settings.LOCATION_IQ_URL.format(location=self._location)
        response: list = make_request(url)
        assert len(response) > 0, 'Location not found'
        result: dict = response[0]

        # Make request to dark sky api
        url = settings.DARK_SKY_URL.format(lat=result.get('lat'), lon=result.get('lon'))
        response: dict = make_request(url)
        return response

    def data(self):
        """
        Get grabbed data from apis
        :return: :Weather: model attributes dictionary
        """
        response = self.get_forecast()

        current_data = response['currently']
        daily_data = response['daily']['data']

        # Api return first item as current day
        daily: dict = daily_data[0]

        temperature = current_data['temperature']
        daily_low = daily['temperatureLow']
        daily_min = daily['temperatureHigh']
        weekly_min = min(map(lambda it: it['temperatureLow'], daily_data))
        weekly_max = max(map(lambda it: it['temperatureHigh'], daily_data))

        # these keys unpacked in models, should be same as models attribute
        return {
            'temperature': temperature,
            'today_lowest': daily_low,
            'today_highest': daily_min,
            'weekly_lowest': weekly_min,
            'weekly_highest': weekly_max,
        }
