import logging

from django.conf import settings
from django.core.cache import cache
from rest_framework.response import Response
from rest_framework.views import set_rollback

logger = logging.getLogger('kds')

DEBUG = settings.DEBUG


class CacheHandler:
    """
    A familiar cache handler

    >>> CacheHandler.set('test', 123)
    >>> CacheHandler.get('test')
    >>> CacheHandler.update('test')
    """

    @staticmethod
    def get(key):
        return cache.get(key)

    @staticmethod
    def set(key, value):
        cache.set(key, value, timeout=settings.CACHE_QUERY_TIMEOUT)

        # note: possible async order issue, but delete anyway!
        cache.delete_many(cache.keys('*')[50:])

    @staticmethod
    def update(key):
        cache.expire(key, timeout=settings.CACHE_QUERY_TIMEOUT)


def api_exception_handler(exc, context):
    """Custom django rest exception handler"""
    if not getattr(exc, 'status_code', None):
        setattr(exc, 'status_code', 500)

    error_data = {
        "error": True,
        "message": str(exc),
        "status_code": exc.status_code
    }
    set_rollback()
    logger.error(error_data)
    return Response(error_data, status=exc.status_code)
