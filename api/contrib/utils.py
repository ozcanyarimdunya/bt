import json
from urllib.request import urlopen


def make_request(url, serialize=True) -> (str, list, dict):
    """
    Make a simple third party api request
    requests library can be used if request goes advanced(with proxy, auth etc.)
    https://github.com/psf/requests

    ... Note
    requests library is installed with `drf-yasg` module for swagger api
    This method will be deprecated in `v0.1.1` and requests library will be used instead of urllib
    :param url:
    :param serialize: whether data convert to python list/dict or string
    :return: api response as str/list/dict
    """
    req = urlopen(url)
    response = req.read().decode('utf-8')
    if serialize:
        return json.loads(response)
    return response

