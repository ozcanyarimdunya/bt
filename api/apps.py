from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'api'

    def ready(self):
        """
        Django should be aware of model signal if signals written in another file
        :return:
        """
        from api.signals import post_create_weather  # noqa
