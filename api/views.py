from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema as api_doc
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from api.exceptions import EmptyQueryException
from api.models import Weather
from api.serializers import WeatherSerializers


class SearchAPIView(APIView):
    """Search APIView

    Request
        GET /api/search/?location=xxx
    Response
        {
            "temperature": "",
            "today_lowest": "",
            "today_highest": "",
            "weekly_lowest": "",
            "weekly_highest": ""
        }
    """

    @api_doc(
        operation_description="Get a location's weather",
        manual_parameters=[
            openapi.Parameter('location', openapi.IN_QUERY, type=openapi.TYPE_STRING)
        ]
    )
    def get(self, request):
        """Search a location with its name"""
        location = request.GET.get('location', None)
        self.perform_validation(location)
        weather = Weather.objects.search(location=location, user=request.user)
        serializer = WeatherSerializers(weather)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @staticmethod
    def perform_validation(location: str):
        """Perform an null check for query string"""
        if (location is None
                or not location.strip()):
            raise EmptyQueryException
