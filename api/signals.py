from django.db.models.signals import post_save
from django.dispatch import receiver

from api.contrib.handlers import CacheHandler
from api.models import Weather


@receiver(post_save, sender=Weather)
def post_create_weather(sender, instance: Weather, created, **kwargs):
    """
    Save created or updated instance to cache
    Triggered when weather objects created
    This is an optional method, we could use this method is `WeatherManager:search` method
    :param sender:
    :param instance:
    :param created:
    :param kwargs:
    :return:
    """
    CacheHandler.set(instance.location, instance)
