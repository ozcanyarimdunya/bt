# BtWeather

[![Coverage Status](https://coveralls.io/repos/gitlab/ozcanyarimdunya/bt/badge.svg?branch=HEAD)](https://coveralls.io/gitlab/ozcanyarimdunya/bt?branch=HEAD)

A simple web api for weather forecast.

## Contents

- [Introduction](#introduction)
- [Specifications](#specifications)
- [Architecture](#architecture)
    - [System architecture](#system-architecture)
    - [Project architecture](#project-architecture)
- [Structure](#structure)
- [Installation](#installation)
- [Usage](#usage)
- [Tests and coverage](#tests-and-coverage)
- [Deployment](#deployment)
- [Licence](#licence)
- [Algorithm Question](#algorithm-question)

## Introduction

Application aim to provide following information to clients,

- Current temperature
- Daily expected lowest and highest temperature
- Weekly expected lowest and highest temperature

with a location/city name as input.

It uses following 3rd party api to provide data,

- Location iq: Latitude and Longitude of location
- Dark sky: Temperature related information of latitude and longitude

## Specifications

This is django application with the relational database and cache.

Data comes from 3rd party api requests, cached(key: location) and stored in a database.

- if new location query cached(max: 50 keys) then data returns from cache
- if new location query is not exists in cache but stored in the database of same day then data returns from the
  database and saved to cache
- If new location query is not exists in the cache, and the database then a new request made to the 3rd party api and
  stored in both cache and database

**Cache**:

- Redis
- key is the location
- expire time is 1 hour
- max stored keys in 50

**Database**

- Postgres
- A Weather model to store 3rd party api response
- index for model is location

## Architecture

Application built with below os/technologies/tools/frameworks/any things

- Python 3.6+
- PostgreSQL
- Redis
- Nginx
- Docker and docker-compose
- Django and django-rest-framework
- Swagger
- Coverage
- Git and Gitlab
- Makefile
- Windows 10 Pro
- PyCharm

### System architecture

![System architecture](assets/SystemArch.png)

### Project architecture

![Project architecture](assets/Arch.png)

## Structure

| File/Folder         | Description                                            |
| ------------------- | ------------------------------------------------------ |
| api/                | Django api application                                 |
| bt/                 | Django settings and other meta files                   |
| scripts/            | *Deployment* - Deployment files                        |
| docker-compose.yaml | *Deployment* - Main docker-compose file for deployment |
| Dockerfile          | *Deployment* - Dockerfile for webapp container         |

## Installation

Make sure you have installed a python with version 3.6+.

> This project developed on Windows 10 Pro 64 bit, if you are on another os, then use `make` instead of `.\make.cmd` .

1. Clone the project

```shell
git clone https://gitlab.com/ozcanyarimdunya/bt.git
cd bt/
```

2. Create a virtual environment and activate it

```shell
python -m venv c:\path\to\myenv
c:\path\to\myenv\Scripts\activate.bat
```

3. Install the requirements

```shell
.\make.cmd install
```

4. Setup dependencies, [postgres](https://www.postgresql.org/download/windows/) and [redis](https://redis.io/download).

```
Remember to set postgres password same as password in bt/settings/dev.py
```

5. Run

```shell
.\make.cmd run
```

Application's current version is: **0.1.0**, check the version

```shell
python -c "import bt; print(bt.__version__)"
```

## Usage

Create an initial superuser with credentials

- username: **admin**
- password: **123**

by running below command

```shell
.\make.cmd loaddata
```

1. Obtain JWT Auth Token

```shell
curl -X POST "http://127.0.0.1:8000/api/login/" \
	-H  "accept: application/json" \
	-H  "Content-Type: application/json" \
	-d  '{  "username": "admin",  "password": "123"}'
```

2. Use this token as auth header and request to endpoints

```shell
curl -X GET "http://127.0.0.1:8000/api/search/?location=<LOCATION>" \
	-H  "accept: application/json" \
	-H  "Authorization: JWT <TOKEN>"
```

3. Or simple go to http://127.0.0.1:8000/ and use the swagger ui.

## Tests and coverage

Application uses builtin django tests.

1. Test

```shell
.\make.cmd test
```

2. Coverage

```shell
.\make.cmd coverage
```

## Deployment

Make sure you have installed the docker and docker-compose.

```shell
docker-compose up -d --build
```

Application will be served at http://<ip>:8888/

## Licence

```
MIT License

Copyright (c) 2021 ÖZCAN YARIMDÜNYA @ozcanyarimdunya

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

---

## Algorithm Question

If one shake his hand with another drop by 1, `2` is a special case.

```python

def handshake(n, result={}):
    """
    If one shake his hand with another drop by 1
    `2` is a special case
    :param n:
    :param result:
    :return:
    """
    if n == 0 or n == 1 or n == 2:
        return 1

    if result.get(n):
        return result[n]

    result[n] = handshake(n - 1, result) + (n - 1)
    return result[n]


if __name__ == '__main__':
    assert handshake(0) == 1
    assert handshake(1) == 1
    assert handshake(2) == 1
    assert handshake(3) == 3
    assert handshake(4) == 6
    assert handshake(5) == 10
    assert handshake(100) == 4950
    assert handshake(1000) == 499500

```
